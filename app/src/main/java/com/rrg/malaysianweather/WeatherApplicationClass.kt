package com.rrg.malaysianweather

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp //activate dagger and hilt
class WeatherApplicationClass : Application() {
}