package com.rrg.malaysianweather.data.Locations

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LocationDetails(
    val id: String,
    val name :String,
    val locationcategoryid :String,
    val locationrootid :String,
    val latitude:Double,
    val longitude:Double
):Parcelable
