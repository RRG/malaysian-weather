package com.rrg.malaysianweather.data.enums

enum class LocationTypes {
    STATE,DISTRICT,TOWN,TOURISTDEST,WATERS
}