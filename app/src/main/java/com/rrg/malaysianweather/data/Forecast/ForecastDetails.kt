package com.rrg.malaysianweather.data.Forecast

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ForecastDetails(
    val locationid: String,
    val locationname :String,
    val locationcategoryid :String,
    val locationrootid :String,
    val date: String,
    val datatype: String,
    val value: String,
    val latitude:Double,
    val longitude:Double
): Parcelable
