package com.rrg.malaysianweather.data.Room

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "LocationRoomDatabase")
@Parcelize
data class LocationTable(
    @PrimaryKey val id:String,
    val name:String,
    val selected:Boolean
): Parcelable
