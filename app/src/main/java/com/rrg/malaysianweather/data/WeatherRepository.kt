package com.rrg.malaysianweather.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.rrg.malaysianweather.api.weatherAPI
import com.rrg.malaysianweather.data.Locations.LocationPagingSource
import com.rrg.malaysianweather.data.enums.Datacategoryid
import com.rrg.malaysianweather.data.enums.Languages
import com.rrg.malaysianweather.data.enums.LocationTypes
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WeatherRepository @Inject constructor(
    private val weatherApi: weatherAPI
    ) {

    fun getLocationResults(locationType: LocationTypes) =
        Pager(
            config = PagingConfig(
                pageSize = 20,
                maxSize = 100,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { LocationPagingSource(weatherApi, locationType) }
        ).liveData

    suspend fun getForecast(locationID: String, startDate:String, endDate: String, lang:Languages )=
        weatherApi.getGeneralForecast(locationID,startDate,endDate,lang)

    suspend fun getWarnings(dataCategoryId: Datacategoryid,startDate:String, endDate: String) =
        weatherApi.getWarning(dataCategoryId,startDate,endDate)

}