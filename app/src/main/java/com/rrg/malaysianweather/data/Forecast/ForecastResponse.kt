package com.rrg.malaysianweather.data.Forecast

import android.os.Parcelable
import com.rrg.malaysianweather.data.Locations.LocationDetails
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ForecastResponse(
    val metadata: resultset,
    val results: List<ForecastDetails>
): Parcelable {

    @Parcelize
    data class resultset(
        val count: Int,
        val offset: Int,
        val limit: Int
    ): Parcelable
}
