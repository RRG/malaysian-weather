package com.rrg.malaysianweather.data.Room

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.rrg.malaysianweather.di.ApplicationScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Provider

@Database(entities = [LocationTable::class],version = 1)
abstract class LocationDatabase : RoomDatabase() {

    abstract fun locationDao(): LocationDAO

    class Callback @Inject constructor(
        private val database: Provider<LocationDatabase>,
        @ApplicationScope private val appScope: CoroutineScope
    ): RoomDatabase.Callback(){

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)

            val dao = database.get().locationDao()

            appScope.launch {
                dao.insert(LocationTable("LOCATION:340","KUALA LUMPUR",selected = true))
            }

        }
    }

}