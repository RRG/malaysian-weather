package com.rrg.malaysianweather.data.Locations

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.rrg.malaysianweather.api.weatherAPI
import com.rrg.malaysianweather.data.enums.LocationTypes
import retrofit2.HttpException
import java.io.IOException

private const val WEATHER_STARTING_PAGE=0

class LocationPagingSource(
    private val weatherAPI: weatherAPI,
    private val locationType: LocationTypes
) :PagingSource<Int, LocationDetails>(){

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, LocationDetails> {
        val position = params.key ?: WEATHER_STARTING_PAGE


        return try {
            //load size ??
            val response = weatherAPI.getLocations(locationType, position)
            val locations = response.results


            LoadResult.Page(
                data = locations,
                prevKey = if (position == WEATHER_STARTING_PAGE) null else position -50,
                nextKey = if(locations.isEmpty()) null else position +50
            )
        } catch (e:IOException){
            LoadResult.Error(e)
        }catch (e:HttpException){
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, LocationDetails>): Int? {
        return state.anchorPosition
    }


}