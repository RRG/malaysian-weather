package com.rrg.malaysianweather.data.condensedForecast

data class CondensedForecast(
    var id: String,
    var location: String,
    var maxTemp: String,
    var minTemp: String,
    var weather_morning: String,
    var morning_img: Int,
    var weather_afternoon: String,
    var afternoon_img: Int,
    var weather_night: String,
    var night_img: Int,
    var weather_significant:String,
    var significant_img: Int,
    var date: String,
    var selected: Boolean,
){
    constructor():this(
        "",
        "",
        "",
        "",
        "",
        0,
        "",
        0,
        "",
        0,
        "",
        0,
        "",
        false
    )
}
