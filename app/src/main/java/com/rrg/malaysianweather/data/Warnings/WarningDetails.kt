package com.rrg.malaysianweather.data.Warnings

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.time.LocalDateTime

@Parcelize
data class WarningDetails(
    val date: String,
    val datatype: String,
    val value: Value,
    val attributes: Attributes
): Parcelable{

    @Parcelize
    data class Value(
        val heading :Heading,
        val text : Text
    ):Parcelable {

        @Parcelize
        data class Heading(
            val en: String,
            val ms: String
        ) : Parcelable

        @Parcelize
        data class Text(
            val en: En,
            val ms: Ms
        ) : Parcelable {

            @Parcelize
            data class En(
                val earthquake: String,
                val tsunami: String,
                val warning:String
            ) : Parcelable

            @Parcelize
            data class Ms(
                val earthquake: String,
                val tsunami: String,
                val warning:String
            ) : Parcelable
        }
    }

    @Parcelize
    data class Attributes(
        val title: Title
    ) : Parcelable {

        @Parcelize
        data class Title(
            val en: String,
            val ms: String
        ) : Parcelable
    }
}
