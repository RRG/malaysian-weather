package com.rrg.malaysianweather.data.Locations

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LocationResponse(
    val metadata: resultset,
    val results: List<LocationDetails>
):Parcelable {

    @Parcelize
    data class resultset(
        val count: Int,
        val offset: Int,
        val limit: Int
    ):Parcelable
}