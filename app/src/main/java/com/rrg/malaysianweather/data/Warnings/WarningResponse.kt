package com.rrg.malaysianweather.data.Warnings

import android.os.Parcelable
import com.rrg.malaysianweather.ui.Warning.Warning
import kotlinx.android.parcel.Parcelize


@Parcelize
data class WarningResponse(
    val metadata: resultset,
    val results: List<WarningDetails>
): Parcelable {

    @Parcelize
    data class resultset(
        val count: Int,
        val offset: Int,
        val limit: Int,
        val datacategoryid:String
    ): Parcelable

}