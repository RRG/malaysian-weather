package com.rrg.malaysianweather.data.Room

import androidx.lifecycle.LiveData
import androidx.room.*
import com.rrg.malaysianweather.data.Locations.LocationDetails


@Dao
interface LocationDAO {

    @Query("SELECT * FROM LocationRoomDatabase")
    fun getSavedLocations():LiveData<List<LocationTable>>

    @Query("SELECT * FROM LocationRoomDatabase WHERE selected = 1")
    fun getSelectedLocation():LiveData<LocationTable>

    @Query("UPDATE LocationRoomDatabase SET selected = 0")
    suspend fun unSelectAllLocations()

    @Query("UPDATE LocationRoomDatabase SET selected = 1 WHERE name = :locationName")
    suspend fun selectLocation(locationName:String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(location : LocationTable)

    @Update
    suspend fun update(location: LocationTable)

    @Delete
    suspend fun delete(location : LocationTable)

}