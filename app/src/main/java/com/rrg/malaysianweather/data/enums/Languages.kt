package com.rrg.malaysianweather.data.enums

enum class Languages {
    //en -> english, ms-> malay
    en,ms
}