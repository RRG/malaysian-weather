package com.rrg.malaysianweather.data.enums

enum class TimesOfDay {
    MORNING,AFTERNOON,NIGHT
}