package com.rrg.malaysianweather.di

import android.app.Application
import androidx.room.Room
import com.rrg.malaysianweather.api.weatherAPI
import com.rrg.malaysianweather.data.Room.LocationDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import javax.inject.Qualifier
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule{

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(weatherAPI.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideWeatherAPI(retrofit: Retrofit): weatherAPI =
        retrofit.create(weatherAPI::class.java)

    @ApplicationScope
    @Provides
    @Singleton
    fun provide_app_scope() = CoroutineScope(SupervisorJob())
    //coroutine scope created for as long as the application lives

    @Provides
    @Singleton
    fun provideDatabase(
        app:Application,
        callback: LocationDatabase.Callback
    ) = Room.databaseBuilder(app, LocationDatabase::class.java,"SAVED_LOCATIONS")
            .fallbackToDestructiveMigration()
            .addCallback(callback)  //init database with locations
            .build()

    @Provides
    fun provideDatabaseDAO(
        database: LocationDatabase
    ) = database.locationDao()

}

@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class ApplicationScope