package com.rrg.malaysianweather.api

import com.rrg.malaysianweather.BuildConfig
import com.rrg.malaysianweather.data.Forecast.ForecastResponse
import com.rrg.malaysianweather.data.enums.Languages
import com.rrg.malaysianweather.data.Locations.LocationResponse
import com.rrg.malaysianweather.data.Warnings.WarningResponse
import com.rrg.malaysianweather.data.enums.Datacategoryid
import com.rrg.malaysianweather.data.enums.LocationTypes
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface weatherAPI {

    companion object{
        const val BASE_URL = "https://api.met.gov.my/v2.1/"
        const val API_KEY = BuildConfig.WEATHER_API_ACCESS_KEY // when key is put in gradle file
    }

    @Headers("Authorization: $API_KEY")
    @GET("locations")
    suspend fun getLocations(
        @Query("locationcategoryid") locationID: LocationTypes,
        @Query("offset")offset:Int
    ): LocationResponse


    @Headers("Authorization: $API_KEY")
    @GET("data?datasetid=FORECAST&datacategoryid=GENERAL")
    suspend fun getGeneralForecast(
        @Query("locationid") locationID:String,
        @Query("start_date") start_date:String,
        @Query("end_date") end_date:String,
        @Query("lang") language: Languages
    ): ForecastResponse


    @Headers("Authorization: $API_KEY")
    @GET("data?datasetid=WARNING")
    suspend fun getWarning(
        @Query("datacategoryid") dataCategoryID: Datacategoryid,
        @Query("start_date") start_date:String,
        @Query("end_date") end_date:String
    ): WarningResponse

}