package com.rrg.malaysianweather.ui.GeneralAlerts

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlin.system.exitProcess

class GeneralAlertDialogViewModel() :ViewModel(){

    fun onConfirmClick() {

    }

    fun closeApp() {
        android.os.Process.killProcess(android.os.Process.myPid());
        exitProcess(1);
    }

}