package com.rrg.malaysianweather.ui.Radar

import android.media.AudioRecord.MetricsConstants.SOURCE
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target;
import com.rrg.malaysianweather.R
import com.rrg.malaysianweather.databinding.RadarImagesFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RadarImages : Fragment(R.layout.radar_images_fragment) {

    companion object {
        val SATELLITE_IMAGE = "https://api.met.gov.my/static/images/satelit-latest.gif"
        val RADAR_IMAGE = "https://api.met.gov.my/static/images/radar-latest.gif"
        val FORECAST_IMAGE = "https://api.met.gov.my/static/images/swirl-latest.gif"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = RadarImagesFragmentBinding.bind(view)

        binding.apply {
            toggleButtonRadar.addOnButtonCheckedListener{ group, checkedId, isChecked ->
                when(checkedId){
                    R.id.btn_radar_satellite -> {
                        Glide.with(requireContext())
                            .load(SATELLITE_IMAGE)
                            .apply(RequestOptions()
                                .fitCenter()
                                .format(DecodeFormat.PREFER_ARGB_8888)
                                .override(Target.SIZE_ORIGINAL))
                            .placeholder(R.drawable.ic_download_glide)
                            .error(R.drawable.ic_baseline_error_24)
                            .fallback(R.drawable.ic_fallback_glide)
                            .into( ivRadarContent)
                    }
                    R.id.btn_radar_radar -> {
                        Glide.with(requireContext())
                            .load(RADAR_IMAGE)
                            .apply(
                                RequestOptions()
                                .fitCenter()
                                .format(DecodeFormat.PREFER_ARGB_8888)
                                .override(Target.SIZE_ORIGINAL))
                            .placeholder(R.drawable.ic_download_glide)
                            .error(R.drawable.ic_baseline_error_24)
                            .fallback(R.drawable.ic_fallback_glide)
                            .into( ivRadarContent)
                    }
                    R.id.btn_radar_forecast -> {
                        Glide.with(requireContext())
                            .load(FORECAST_IMAGE)
                            .apply(RequestOptions()
                                .fitCenter()
                                .format(DecodeFormat.PREFER_ARGB_8888)
                                .override(Target.SIZE_ORIGINAL))
                            .placeholder(R.drawable.ic_download_glide)
                            .error(R.drawable.ic_baseline_error_24)
                            .fallback(R.drawable.ic_fallback_glide)
                            .into( ivRadarContent)
                    }
                }
            }
        }

    }

}