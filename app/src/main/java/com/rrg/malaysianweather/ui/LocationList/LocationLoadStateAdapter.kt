package com.rrg.malaysianweather.ui.LocationList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rrg.malaysianweather.databinding.LoadingFooterBinding

class LocationLoadStateAdapter (private val retry:() ->Unit) :
    LoadStateAdapter<LocationLoadStateAdapter.LoadStateViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadStateViewHolder {
        val binding = LoadingFooterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LoadStateViewHolder(binding)
    }
    override fun onBindViewHolder(holder: LoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    inner class LoadStateViewHolder(private val binding: LoadingFooterBinding) : RecyclerView.ViewHolder(binding.root){

        init{
            binding.btnLocationRetry.setOnClickListener {
                retry.invoke()
            }
        }

        fun bind(loadState: LoadState){
             binding.apply {
                 locationProgress.isVisible = loadState is LoadState.Loading
                 btnLocationRetry.isVisible = loadState !is LoadState.Loading
                 tvLocationError.isVisible = loadState !is LoadState.Loading
             }
        }
    }
}