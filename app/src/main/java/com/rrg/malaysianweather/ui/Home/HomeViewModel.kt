package com.rrg.malaysianweather.ui.Home

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.lifecycle.*
import com.rrg.malaysianweather.R
import com.rrg.malaysianweather.data.Forecast.ForecastResponse
import com.rrg.malaysianweather.data.Room.LocationDAO
import com.rrg.malaysianweather.data.WeatherRepository
import com.rrg.malaysianweather.data.condensedForecast.CondensedForecast
import com.rrg.malaysianweather.data.enums.Languages
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


@HiltViewModel
class HomeViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository,
    private val locationDAO: LocationDAO
) : ViewModel() {

    val selectedLocation = locationDAO.getSelectedLocation()
    val allSavedLocations = locationDAO.getSavedLocations()

    private val weekDates:ArrayList<String> = getWeekDates()

    private val list = MutableLiveData<List<CondensedForecast>>()
    val weatherForecast: LiveData<List<CondensedForecast>> = list

    fun getForecast(locationID: String) = viewModelScope.launch {
                //default language to english for now
                list.value = processWeatherData(weatherRepository.getForecast(
                                                                locationID,
                                                                weekDates[0],
                                                                weekDates[weekDates.size - 1],
                                                                Languages.en
                                                            ))
                _isLoading.value = false
    }


    val _isLoading = MutableLiveData(true)


    private fun processWeatherData(value: ForecastResponse): ArrayList<CondensedForecast> {
        val futureForecast: ArrayList<CondensedForecast> = ArrayList()
        val forecastResults = value.results


        for (days in weekDates) {
            val temp = CondensedForecast()

            for (results in forecastResults) {
                val formatted_date = results.date
                    .replace('T', ' ')
                    .split(' ')[0]
                if (formatted_date == days) {
                    when (results.datatype) {
                        "FGM" -> {
                            temp.weather_morning = results.value
                            temp.morning_img = assignImages(results.value, "Morning")
                            temp.id = results.locationid
                            temp.location = results.locationname.toLowerCase(Locale.ROOT)
                            temp.date = days
                        }
                        "FGA" -> {
                            temp.weather_afternoon = results.value
                            temp.afternoon_img = assignImages(results.value, "Afternoon")
                        }

                        "FGN" -> {
                            temp.weather_night = results.value
                            temp.night_img = assignImages(results.value, "Night")
                        }

                        "FMINT" -> {
                            temp.minTemp = results.value
                        }

                        "FMAXT" -> {
                            temp.maxTemp = results.value
                        }

                        "FSIGW" -> {
                            temp.weather_significant = results.value
                            temp.significant_img = assignImages(results.value, "Afternoon")
                        }

                    }
                }

            }
            futureForecast.add(temp)
        }
        futureForecast[0].selected = true
        return futureForecast
    }


    private fun getWeekDates(): ArrayList<String> {
        val temp:ArrayList<String> = ArrayList()
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val cal = Calendar.getInstance()
        temp.add(sdf.format(cal.time))
        for(i in 1..5){
            cal.add(Calendar.DATE, 1)
            temp.add(sdf.format(cal.time))
        }
        return temp
    }

    private fun assignImages(value: String, time: String): Int {
        var resourceID = 0
        var newValue = ""
        if(value.contains("no rain", true)){
            newValue = "no rain"
        }else if (value.contains("rain", true)){
            newValue = "rain"
        }else if (value.contains("thunderstorms", true)){
            newValue = "thunderstorms"
        }else{
            newValue = "cloudy"
        }

        when(newValue) { //because of inconsistency with api result
            "no rain" -> {
                when (time) {
                    "Morning" -> {
                        resourceID = (R.drawable.ic__39_sun)
                    }
                    "Afternoon" -> {
                        resourceID = (R.drawable.ic__15_day)
                    }
                    else -> {
                        resourceID = (R.drawable.ic__22_night_3)
                    }
                }
            }

            "rain" -> {
                when (time) {
                    "Morning" -> {
                        resourceID = (R.drawable.ic__34_cloudy_1)
                    }
                    "Afternoon" -> {
                        resourceID = (R.drawable.ic__03_rainy)
                    }
                    else -> {
                        resourceID = (R.drawable.ic__21_night_2)
                    }
                }
            }

            "thunderstorms" -> {
                when (time) {
                    "Morning" -> {
                        resourceID = (R.drawable.ic__36_storm_4)
                    }
                    "Afternoon" -> {
                        resourceID = (R.drawable.ic__13_storm_2)
                    }
                    else -> {
                        resourceID = (R.drawable.ic__20_storm_3)
                    }
                }
            }

            "cloudy" -> {
                when (time) {
                    "Morning" -> {
                        resourceID = (R.drawable.ic__38_cloudy_3)
                    }
                    "Afternoon" -> {
                        resourceID = (R.drawable.ic__11_cloudy)
                    }
                    else -> {
                        resourceID = (R.drawable.ic__02_cloud_1)
                    }
                }
            }
        }
        return resourceID
    }

    
    /////saved locations
    val savedLocations = locationDAO.getSavedLocations()

    fun getConnectionType(context: Context): Int {
        var result = 0 // Returns connection type. 0: none; 1: mobile data; 2: wifi
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    result = 2
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    result = 1
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN)) {
                    result = 3
                }
            }
        } else {
            val activeNetwork = cm.activeNetworkInfo
            if (activeNetwork != null) {
                // connected to the internet
                if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                    result = 2
                } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                    result = 1
                } else if (activeNetwork.type == ConnectivityManager.TYPE_VPN) {
                    result = 3
                }
            }
        }
        return result
    }

}
