package com.rrg.malaysianweather.ui.GeneralAlerts

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class GeneralAlertDialog :DialogFragment() {

    private val viewModel: GeneralAlertDialogViewModel by viewModels()

    private val args by navArgs<GeneralAlertDialogArgs>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        AlertDialog.Builder(requireContext())
            .setTitle("Error")
            .setMessage(args.errorMessage)
            .setNeutralButton("Cancel") { _, _ ->
                viewModel.onConfirmClick()
                dismiss()
            }
            .setNegativeButton("Close App") { _, _ ->
                viewModel.closeApp()
            }
            .create()
}