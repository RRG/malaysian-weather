package com.rrg.malaysianweather.ui.Home


import android.content.Context
import android.net.ConnectivityManager
import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.AnimationUtils.loadAnimation
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rrg.malaysianweather.R
import com.rrg.malaysianweather.data.Room.LocationTable
import com.rrg.malaysianweather.data.enums.TimesOfDay
import com.rrg.malaysianweather.databinding.HomeFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.home_fragment.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


@AndroidEntryPoint
class Home : Fragment(R.layout.home_fragment), ForecastAdapter.OnClickListener {

    private val viewModel by viewModels<HomeViewModel>()

    var spf = SimpleDateFormat("yyyy-MM-dd")
    var spf_pretty = SimpleDateFormat("EEE, d MMM yyyy")
    var spf_day = SimpleDateFormat("EEE")

    private lateinit var savedLocations :List<LocationTable>

    private var currentDate_live : MutableLiveData<Int> = MutableLiveData()

    private var timeOfDay : MutableLiveData<TimesOfDay> = MutableLiveData(TimesOfDay.MORNING)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //init my mutable lists
        currentDate_live.value = 0
        timeOfDay.value = TimesOfDay.MORNING

        val binding = HomeFragmentBinding.bind(view)
        val adapter = ForecastAdapter(this)

        when(viewModel.getConnectionType(requireContext())){
            0 ->{
                val action =
                    HomeDirections.actionGlobalGeneralAlertDialog(
                        "No Internet Connection !\n\nThe app can not operate without an internet connection")
                findNavController().navigate(action)
            }
            else->{
                val fadeIn_anim: Animation = loadAnimation(context, R.anim.fadein);

                viewModel.selectedLocation.observe(viewLifecycleOwner){
                    if(it!=null){
                        viewModel.getForecast(it.id)
                    }else{
                        viewModel.getForecast("LOCATION:340") //always default to KL
                    }
                }

                viewModel.allSavedLocations.observe(viewLifecycleOwner){
                    savedLocations = it
                }

                binding.apply {


                    ibtnAddLocation.setOnClickListener {
                        val action = HomeDirections.actionHomePageToLocationList(savedLocations.toTypedArray())
                        findNavController().navigate(action);
                    }

                    tvCurrentLocation.setOnClickListener {
                        val action = HomeDirections.actionGlobalSavedLocations()
                        findNavController().navigate(action)
                    }

                    ivHomeImage.animation = fadeIn_anim

                    toggleButton.check(btnMorningForecast.id)
                    toggleButton.addOnButtonCheckedListener{ group, checkedId, isChecked ->
                        when(checkedId){
                            R.id.btn_morning_forecast -> {
                                timeOfDay.value = TimesOfDay.MORNING
                            }
                            R.id.btn_afternoon_forecast -> {
                                timeOfDay.value = TimesOfDay.AFTERNOON
                            }
                            R.id.btn_night_forecast -> {
                                timeOfDay.value = TimesOfDay.NIGHT
                            }
                        }
                    }

                    currentDate_live.observe(viewLifecycleOwner){

                        val currentDate = it ?: 0

                        viewModel.weatherForecast.observe(viewLifecycleOwner){ response ->

                            if (response[0].id.isBlank()){
                                val action =
                                    HomeDirections.actionGlobalGeneralAlertDialog(
                                        "Meteorological station did not return any result. \n\n"+
                                                "Please choose another location.")
                                findNavController().navigate(action)
                            }

                            tvCurrentLocation.text = response[currentDate].location+" ᐯ"

                            val date:Date
                            try {
                                date = spf.parse(response[currentDate].date)
                                tvHomeDate.text = spf_pretty.format(date)
                            }catch (e:Exception){

                            }

                            rv_week_forecast.layoutManager = LinearLayoutManager(
                                requireContext(),
                                LinearLayoutManager.HORIZONTAL, false
                            )
                            rv_week_forecast.setHasFixedSize(true)
                            rv_week_forecast.adapter = adapter
                            rvWeekForecast.scrollToPosition(currentDate)

                            val tempString: String = resources.getString(
                                R.string.home_temperature,
                                response[currentDate].minTemp,
                                response[currentDate].maxTemp
                            )
                            tvHomeTemp.text = tempString

                            timeOfDay.observe(viewLifecycleOwner){ time_of_day ->
                                when (time_of_day){
                                    TimesOfDay.MORNING->{
                                        tvHomeForecast.text = response[currentDate].weather_morning
                                        ivHomeImage.setImageResource(response[currentDate].morning_img)
                                    }
                                    TimesOfDay.AFTERNOON->{
                                        tvHomeForecast.text = response[currentDate].weather_afternoon
                                        ivHomeImage.setImageResource(response[currentDate].afternoon_img)
                                    }
                                    TimesOfDay.NIGHT->{
                                        tvHomeForecast.text = response[currentDate].weather_night
                                        ivHomeImage.setImageResource(response[currentDate].night_img)
                                    }
                                }
                            }
                        }
                    }
                }

                viewModel.weatherForecast.observe(viewLifecycleOwner){
                    adapter.submitList(it)
                }
            }
        }


    }


    override fun onForecastClicked(date: String) {
        for(element in viewModel.weatherForecast.value!!){
            if(element.date == date){
                element.selected = true
                currentDate_live.value = viewModel.weatherForecast.value?.indexOf(element) ?: 0
            }else{
                element.selected = false
            }
        }
    }



}