package com.rrg.malaysianweather.ui.Home

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.rrg.malaysianweather.R
import com.rrg.malaysianweather.data.condensedForecast.CondensedForecast
import com.rrg.malaysianweather.databinding.ForecastWeatherCardBinding
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class ForecastAdapter(
    private val click_listener: OnClickListener
) :
    ListAdapter<CondensedForecast, ForecastAdapter.ForecastAdapterViewHolder>(DiffCallback())  {


    interface OnClickListener{
        fun onForecastClicked(date:String)
    }


    inner class ForecastAdapterViewHolder(
        private val binding:ForecastWeatherCardBinding
    ) : RecyclerView.ViewHolder(binding.root){

        var spf = SimpleDateFormat("yyyy-MM-dd")
        var spf_pretty = SimpleDateFormat("EEE, d MMM yyyy")
        var spf_day = SimpleDateFormat("EEE, d")

        init {
            binding.apply {
                clForecastParent.setOnClickListener {
                    val index = bindingAdapterPosition
                    if(index != RecyclerView.NO_POSITION){
                        click_listener.onForecastClicked(binding.tvForecastDay.tag.toString())
                    }
                }
            }
        }

        fun bind(forecasts: CondensedForecast) {
            binding.apply {

                tvForecastTemp.text = forecasts.maxTemp + "°C"
                val date: Date

                try {
                    date = spf.parse(forecasts.date)
                    tvForecastDay.text = spf_day.format(date)
                }catch (e:Exception){

                }
                tvForecastDay.tag = forecasts.date
                ivForecastImage.setImageResource(forecasts.significant_img)
                if(forecasts.selected) {
                    clForecastParent.setBackgroundResource(R.drawable.main_card_alt)
                }else{
                    clForecastParent.setBackgroundResource(R.drawable.white_backgraound)
                }
            }
        }
    }

    class DiffCallback: DiffUtil.ItemCallback<CondensedForecast>(){
        override fun areItemsTheSame(oldItem: CondensedForecast, newItem: CondensedForecast): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: CondensedForecast,
            newItem: CondensedForecast
        ): Boolean {
            return  oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastAdapterViewHolder {
        val binding = ForecastWeatherCardBinding.inflate(
            LayoutInflater.from(parent.context),parent,false
        )
        return ForecastAdapterViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ForecastAdapterViewHolder, position: Int) {
        val currentItem = getItem(position)
        if (currentItem != null) {
            holder.bind(currentItem)
        }
    }

}