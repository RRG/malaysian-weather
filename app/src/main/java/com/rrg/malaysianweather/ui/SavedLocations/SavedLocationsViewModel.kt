package com.rrg.malaysianweather.ui.SavedLocations

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rrg.malaysianweather.data.Room.LocationDAO
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SavedLocationsViewModel @Inject constructor(
    private val locationDAO: LocationDAO
): ViewModel() {

    val savedLocations = locationDAO.getSavedLocations()

    fun unselectAllLocations() = viewModelScope.launch {
        locationDAO.unSelectAllLocations()
    }

    fun selectLocation(locationName:String) = viewModelScope.launch {
        locationDAO.selectLocation(locationName)
    }

        fun onClick() {

        }

}