package com.rrg.malaysianweather.ui.SavedLocations

import android.app.Dialog
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.rrg.malaysianweather.R
import com.rrg.malaysianweather.data.Room.LocationDAO
import com.rrg.malaysianweather.databinding.SavedLocationListBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class SavedLocations : DialogFragment() {

    private val viewModel: SavedLocationsViewModel by viewModels()
    private var locations = ArrayList<String>(arrayListOf("Loading..."))

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let { it ->
            val builder = AlertDialog.Builder(it, R.style.DialogStyle)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater;
            val v = inflater.inflate(R.layout.saved_location_list, null)
            val binding = SavedLocationListBinding.bind(v)
            val adapter = ArrayAdapter(
                requireContext(),
                R.layout.my_list_view_items,
                locations
            )

            binding.apply {
                lvSavedLocations.adapter = adapter

                lvSavedLocations.setOnItemClickListener { parent, view, position, id ->
                    viewModel.unselectAllLocations()
                    viewModel.selectLocation(locations[position])
                    findNavController().navigateUp()
                }

            }

            viewModel.savedLocations.observe(this) {
                if(it.isEmpty()){
                    locations[0] = "No Records Found"
                }else{
                    locations.clear()
                    for (element in it) {
                        locations.add(element.name)
                    }
                }
                adapter.notifyDataSetChanged()
            }

            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            builder.setView(v)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }


}