package com.rrg.malaysianweather.ui.Settings

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.rrg.malaysianweather.R
import com.rrg.malaysianweather.ui.LocationList.LocationAdapter
import com.rrg.malaysianweather.databinding.SettingsFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class Settings : Fragment(R.layout.settings_fragment) {

    private val viewModel by viewModels<SettingsViewModel>()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }



}