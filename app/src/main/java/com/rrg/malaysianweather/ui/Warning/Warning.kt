package com.rrg.malaysianweather.ui.Warning

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.rrg.malaysianweather.R
import com.rrg.malaysianweather.databinding.WarningFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class Warning : Fragment(R.layout.warning_fragment) {

    private val viewModel:WarningViewModel by viewModels()

    val data: HashMap<String, List<String>>
        get() {
            val listData = HashMap<String, List<String>>()
            listData["Rain"] = ArrayList<String>()
            listData["Cyclone"] = ArrayList<String>()
            listData["Thunderstorm"] = ArrayList<String>()
            listData["Rough Seas & wind"] = ArrayList<String>()
            listData["Earthquake & Tsunami"] = ArrayList<String>()
            return listData
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding =WarningFragmentBinding.bind(view)

        val listData = data
        val titleList = ArrayList(listData.keys)
        val myadapter = WarningExpandableAdapter(requireContext(), titleList as ArrayList<String>, listData)


        binding.apply {
            elvWarnings.setAdapter(myadapter)

            when(viewModel.getConnectionType(requireContext())){
                0 ->{
                    val action = WarningDirections.actionGlobalGeneralAlertDialog(
                        "No Internet Connection !\n\nThe app can not operate without an internet connection")
                    findNavController().navigate(action)

                }
                else ->{
                    viewModel.rainWarnings.observe(viewLifecycleOwner){
                        listData["Rain"] =it
                    }
                    viewModel.cycloneWarnings.observe(viewLifecycleOwner){
                        listData["Cyclone"] =it
                    }
                    viewModel.thunderWarnings.observe(viewLifecycleOwner){
                        listData["Thunderstorm"] =it
                    }
                    viewModel.seaWarnings.observe(viewLifecycleOwner){
                        listData["Rough Seas & wind"] =it
                    }
                    /*
                    viewModel.thunderWarnings.observe(viewLifecycleOwner){
                        listData["Earthquake & Tsunami"] =it
                    }*/

                }
            }



        }



    }

}