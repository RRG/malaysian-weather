package com.rrg.malaysianweather.ui.Warning

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import androidx.lifecycle.*
import com.rrg.malaysianweather.data.Warnings.WarningResponse
import com.rrg.malaysianweather.data.WeatherRepository
import com.rrg.malaysianweather.data.enums.Datacategoryid
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@HiltViewModel
class WarningViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository
) : ViewModel() {

    private val dates :List<String> = getDates()


    val quaketsunamiWarnings: LiveData<ArrayList<String>> =  MutableLiveData<ArrayList<String>>().apply {
        viewModelScope.launch {
            val result = weatherRepository.getWarnings(Datacategoryid.QUAKETSUNAMI,
                dates[0],dates[1])
            this@apply .postValue(processResults(result))
        }
    }

    val seaWarnings: LiveData<ArrayList<String>> =  MutableLiveData<ArrayList<String>>().apply {
        viewModelScope.launch {
            val result = weatherRepository.getWarnings(Datacategoryid.WINDSEA,
                dates[0],dates[1])
            this@apply .postValue(processResults(result))
        }
    }

    val thunderWarnings: LiveData<ArrayList<String>> =  MutableLiveData<ArrayList<String>>().apply {
        viewModelScope.launch {
            val result = weatherRepository.getWarnings(Datacategoryid.THUNDERSTORM,
                dates[0],dates[1])
            this@apply .postValue(processResults(result))
        }
    }

    val cycloneWarnings: LiveData<ArrayList<String>> =  MutableLiveData<ArrayList<String>>().apply {
        viewModelScope.launch {
            val result = weatherRepository.getWarnings(Datacategoryid.CYCLONE,
                dates[0],dates[1])
            this@apply .postValue(processResults(result))
        }
    }

    val rainWarnings: LiveData<ArrayList<String>> =  MutableLiveData<ArrayList<String>>().apply {
        viewModelScope.launch {
            val result = weatherRepository.getWarnings(Datacategoryid.RAIN,
                dates[0],dates[1])

            this@apply .postValue(processResults(result))
        }
    }

    /*
    private val list= MutableLiveData<ArrayList<String>>()

    val final_list: LiveData<ArrayList<String>> = list

    fun getEverything() = viewModelScope.launch {
        list.value = processResults(weatherRepository.getWarnings(Datacategoryid.THUNDERSTORM,
            dates[0],dates[1]))
    }*/

    private fun getDates(): List<String> {
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.CHINA)
        val cal = Calendar.getInstance()
        val today = sdf.format(cal.time)
        cal.add(Calendar.DATE, 6)
        val future = sdf.format(cal.time)

        return listOf<String>(today,future)
    }

    private fun processResults(result: WarningResponse): ArrayList<String> {
        val res : ArrayList<String> = ArrayList()
        for(element in result.results){
            if (result.metadata.datacategoryid == Datacategoryid.QUAKETSUNAMI.toString()){
                res.add(element.value.heading.en +"\n"+element.value.text.en.earthquake+"\n"+element.value.text.en.tsunami)
            }else{
                res.add(element.value.heading.en +"\n"+element.value.text.en.warning)
            }
        }
        return res
    }



    fun getConnectionType(context: Context): Int {
        var result = 0 // Returns connection type. 0: none; 1: mobile data; 2: wifi
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    result = 2
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    result = 1
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN)) {
                    result = 3
                }
            }
        } else {
            val activeNetwork = cm.activeNetworkInfo
            if (activeNetwork != null) {
                // connected to the internet
                if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                    result = 2
                } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                    result = 1
                } else if (activeNetwork.type == ConnectivityManager.TYPE_VPN) {
                    result = 3
                }
            }
        }
        return result
    }

}