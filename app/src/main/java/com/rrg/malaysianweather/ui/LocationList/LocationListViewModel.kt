package com.rrg.malaysianweather.ui.LocationList

import androidx.lifecycle.*
import androidx.paging.cachedIn
import com.rrg.malaysianweather.data.Locations.LocationDetails
import com.rrg.malaysianweather.data.Room.LocationDAO
import com.rrg.malaysianweather.data.Room.LocationTable
import com.rrg.malaysianweather.data.enums.LocationTypes
import com.rrg.malaysianweather.data.WeatherRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LocationListViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository,
    private val locationDAO: LocationDAO
    ): ViewModel() {

    //default is town list
    private val currentQuery = MutableLiveData(LocationTypes.TOWN)

    val locations = currentQuery.switchMap { locationFilter ->
        weatherRepository.getLocationResults(locationFilter).cachedIn(viewModelScope)
    }


    fun changeLocationFilter(locationType: LocationTypes){
        currentQuery.value = locationType
    }


    fun onClick() {

    }

    fun addLocationToDatastore(data: LocationDetails) = viewModelScope.launch {
        locationDAO.insert(LocationTable(data.id,data.name,false))
    }

    fun removeLocationToDatastore(data: LocationDetails) = viewModelScope.launch {
        locationDAO.delete(LocationTable(data.id,data.name,false))
        locationDAO.delete(LocationTable(data.id,data.name,true))
    }
}