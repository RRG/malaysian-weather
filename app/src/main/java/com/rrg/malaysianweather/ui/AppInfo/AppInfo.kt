package com.rrg.malaysianweather.ui.AppInfo

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rrg.malaysianweather.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AppInfo : Fragment(R.layout.app_info_fragment) {



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

}