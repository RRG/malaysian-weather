package com.rrg.malaysianweather.ui.LocationList

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.rrg.malaysianweather.data.Locations.LocationDetails
import com.rrg.malaysianweather.data.Room.LocationTable
import com.rrg.malaysianweather.databinding.LocationRecordBinding

class LocationAdapter(
    private val myListener: OnClickListener_location,
    val savedLocations: Array<LocationTable>
)
    : PagingDataAdapter<LocationDetails, LocationAdapter.LocationViewHolder>(
    LOCATION_COMPARATOR
) {

    interface OnClickListener_location{
        fun onLocationChecked(location: LocationDetails)
        fun onLocationUnchecked(location: LocationDetails)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewHolder {
        val binding =
            LocationRecordBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LocationViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) {
        val currentItem = getItem(position)
        if (currentItem != null) {
            holder.bind(currentItem)
        }
    }

    inner class LocationViewHolder(private val binding: LocationRecordBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(location: LocationDetails) {
            binding.apply {
                cbLocationItem.text = location.name
                /*
                for(savedLocation in savedLocations){
                    if(location.id==savedLocation.id){
                        Log.i("hehe",location.name+"---"+savedLocation.name)
                        cbLocationItem.isChecked = true
                    }else{
                        cbLocationItem.isChecked = false
                    }
                }*/

                cbLocationItem.setOnCheckedChangeListener { buttonView, isChecked ->
                    val index = bindingAdapterPosition
                    if (index != RecyclerView.NO_POSITION) {
                        if(isChecked){
                            myListener.onLocationChecked(location)
                        }else{
                            myListener.onLocationUnchecked(location)
                        }
                    }

                }
            }
        }
    }

    companion object {
        private val LOCATION_COMPARATOR = object : DiffUtil.ItemCallback<LocationDetails>() {
            override fun areItemsTheSame(oldItem: LocationDetails, newItem: LocationDetails) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: LocationDetails, newItem: LocationDetails) =
                oldItem == newItem

        }
    }
}