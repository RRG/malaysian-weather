package com.rrg.malaysianweather.ui.LocationList

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.rrg.malaysianweather.R
import com.rrg.malaysianweather.data.Locations.LocationDetails
import com.rrg.malaysianweather.data.enums.LocationTypes
import com.rrg.malaysianweather.databinding.LocationsFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LocationList : Fragment(R.layout.locations_fragment),
                        PopupMenu.OnMenuItemClickListener,
                        LocationAdapter.OnClickListener_location {

    private val viewModel by viewModels<LocationListViewModel>()

    private val args by navArgs<LocationListArgs>()

    private var _binding: LocationsFragmentBinding? = null
    private val binding get() = _binding!!


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = LocationsFragmentBinding.bind(view)

        val adapter = LocationAdapter(this,args.savedLocations)

        binding.apply {
            rvLocationList.layoutManager = LinearLayoutManager(requireContext())
            rvLocationList.setHasFixedSize(true)
            rvLocationList.adapter = adapter.withLoadStateHeaderAndFooter(
                header = LocationLoadStateAdapter {adapter.retry()},
                footer = LocationLoadStateAdapter {adapter.retry()}
            )

            btnLocationFilter.setOnClickListener {
                popupMenu(it)
            }

            btnLocationApply.setOnClickListener {
                findNavController().navigateUp()
            }
        }

        viewModel.locations.observe(viewLifecycleOwner) {
            adapter.submitData(viewLifecycleOwner.lifecycle,it)
        }
    }

    private fun popupMenu(view: View) {
        val menu = PopupMenu(context, view)
        menu.setOnMenuItemClickListener(this)
        menu.inflate(R.menu.location_filter)
        menu.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null //avoid mem leak
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.state -> {
                binding.tvCurrentFilter.text = resources.getText(R.string.state)
                viewModel.changeLocationFilter(LocationTypes.STATE)
            }
            R.id.district -> {
                binding.tvCurrentFilter.text = resources.getText(R.string.district)
                viewModel.changeLocationFilter(LocationTypes.DISTRICT)
            }
            R.id.town -> {
                binding.tvCurrentFilter.text = resources.getText(R.string.town)
                viewModel.changeLocationFilter(LocationTypes.TOWN)
            }
            R.id.touristdest -> {
                binding.tvCurrentFilter.text = resources.getText(R.string.tourist_destinations)
                viewModel.changeLocationFilter(LocationTypes.TOURISTDEST)
            }
            else ->{
                return false
            }
        }
        return true
    }

    override fun onLocationChecked(location: LocationDetails) {
        viewModel.addLocationToDatastore(location)
    }

    override fun onLocationUnchecked(location: LocationDetails) {
        viewModel.removeLocationToDatastore(location)
    }

}


